<?php


namespace Kowal\FeedCeneo\Model\Config\Source;

class Configurable implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => 'simple', 'label' => __('simple')],['value' => 'configurable', 'label' => __('configurable')]];
    }

    public function toArray()
    {
        return ['simple' => __('simple'),'configurable' => __('configurable')];
    }
}
