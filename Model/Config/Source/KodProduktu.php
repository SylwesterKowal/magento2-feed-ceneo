<?php


namespace Kowal\FeedCeneo\Model\Config\Source;

class KodProduktu implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => 'sku', 'label' => __('sku')], ['value' => 'product_id', 'label' => __('product_id')]];
    }

    public function toArray()
    {
        return ['sku' => __('sku'), 'product_id' => __('product_id')];
    }
}