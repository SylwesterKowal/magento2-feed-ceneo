<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 14/11/2018
 * Time: 02:08
 */

namespace Kowal\FeedCeneo\lib;


class Queries
{
    protected $_tablePrefix = '';

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection
    )
    {

        $this->connection = $resourceConnection->getConnection();
    }

    public
    function _applyTablePrefix($query)
    {
        return str_replace('PFX_', $this->_tablePrefix, $query);
    }

    public function getSuperAttribiutId($product_id, $tabel = 'catalog_product_super_attribute')
    {
        $query = "SELECT attribute_id FROM PFX_" . $tabel . " WHERE product_id = :product_id";
        $query = $this->_applyTablePrefix($query);
        return $this->connection->fetchOne($query, array('product_id' => $product_id));
    }

    public function getAttribiutValue($attrId, $entity_id, $tabel = 'catalog_product_entity_int')
    {
        $query = "SELECT value FROM PFX_" . $tabel . " WHERE attribute_id = :attribute_id AND entity_id = :entity_id";
        $query = $this->_applyTablePrefix($query);
        return $this->connection->fetchOne($query, array("attribute_id" => $attrId, 'entity_id' => $entity_id));
    }
}