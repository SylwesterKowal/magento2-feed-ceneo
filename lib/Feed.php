<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 15/11/2018
 * Time: 00:09
 */

namespace Kowal\FeedCeneo\lib;

use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\CatalogInventory\Helper\Stock;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Tax\Api\TaxCalculationInterface;

class Feed
{
    public function __construct(
//        \Magento\Framework\App\State $state,
        Settings $settings,
        Queries $queries,
        CollectionFactory $collectionFactory,
        Stock $stockFilter,
        StoreManagerInterface $storeManager,
        DirectoryList $directoryList,
//        \Magento\Framework\App\ResourceConnection $resourceConnection,
        TaxCalculationInterface $taxCalculation,
        ScopeConfigInterface $scopeConfig,
        Configurable $configurable,
        $name = null
    )
    {

        $this->settings = $settings;
        $this->queries = $queries;
        $this->collectionFactory = $collectionFactory;
        $this->taxCalculation = $taxCalculation;
        $this->scopeConfig = $scopeConfig;
        $this->stockFilter = $stockFilter;
        $this->configurable = $configurable;
//        $this->connection = $resourceConnection->getConnection();
        $this->storeManager = $storeManager;
        $this->currentStore = $this->storeManager->getStore();
        $this->baseUrl = $this->currentStore->getBaseUrl();
        $this->mediaDir = $this->currentStore->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'catalog/product';
//        $this->mediaDir = str_replace('pub/', '', $this->currentStore->getBaseUrl(UrlInterface::URL_TYPE_MEDIA)) . 'catalog/product';
        $this->pubDir = $directoryList->getPath('pub');

        $this->head = $this->settings->getHead();
        $this->stringContent = $this->settings->getContent();
        $this->footer = $this->settings->getFooter();

        $this->columnEan = ($this->settings->getKodEAN()) ? $this->settings->getKodEAN() : 'sku';

    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {


        $store_id = 1;
        $my_file = $this->getFile();

        $handle = fopen($my_file, 'a') or die('Cannot open file:  ' . $my_file);

        fwrite($handle, $this->head);

        $collection = $this->collectionFactory->create()
            ->addStoreFilter($store_id)
            ->addAttributeToFilter('visibility', [2, 3, 4])
            ->addAttributeToFilter('ceneo_export', 1)
            ->addAttributeToFilter('status', Status::STATUS_ENABLED)
//            ->addAttributeToFilter('price', ['gt' => 0])
            ->addAttributeToSelect(['entity_id', 'name', 'sku', 'manufacturer', 'url_key', 'tax_class_id',
                'price', 'final_price', 'special_price', 'ceneo_free_shipp',
                'ceneo_avil', 'weight', $this->columnEan, 'description', 'image', 'short_description', 'ceneo_opis', 'ceneo_kategoria']);

//        $this->stockFilter->addInStockFilterToCollection($collection);

        foreach ($collection as $product) {

            if ($product->getTypeId() == 'configurable') {
                $data = $this->getDataArray($product);
                fwrite($handle, $data);
                $child_products = $product->getTypeInstance()->getUsedProducts($product, null);
                if (count($child_products) > 0) {
                    foreach ($child_products as $child) {
                        $data = $this->getDataArray($child, $product);
                        fwrite($handle, $data);
                    }
                }
            } else {
                $data = $this->getDataArray($product);
                fwrite($handle, $data);
            }
        }
        fwrite($handle, $this->footer);
        fclose($handle);
    }


    private function getDataArray($product, $parent = null)
    {
        try {
            $price = round($this->getMinPrice($product), 2);
            $name_= trim($product->getName()) . ' ' . trim($product->getCeneoOpis());
            $suffix = '';
            $replace = [
                '{manufacturer}' => trim($product->getResource()->getAttribute('manufacturer')->getFrontend()->getValue($product)),
                '{name}' =>  substr($name_,0, (strlen($name_) >= 150) ? strpos($name_, ' ', 150) : strlen($name_)),
                '{short_description}' => trim($product->getShortDescription()),
                '{image}' => $this->mediaDir . $product->getImage(),
                '{sku}' => trim($product->getSku()),
                '{manufacturer_code}' => $product->getSku(),
                '{' . $this->columnEan . '}' => $this->getEanCode($product),
                '{description}' => htmlentities($product->getDescription()),
                '{url_key}' => ((is_null($parent)) ? $product->getProductUrl() : $parent->getProductUrl()),
                '{tax_final_price}' => $price,
                '{ceneo_avil}' => $this->getCeneoAvil($product),
                '{weight}' => $this->getCeneoFreeShipping($product),
                '{category}' => $this->getCategoryPath($product)
            ];
            if (!is_null($parent)) {
                $superAttrId = $this->queries->getSuperAttribiutId($parent->getEntityId());
                $replace['{url_key}'] = $replace['{url_key}'] . '#' . $superAttrId . '=' . $this->queries->getAttribiutValue($superAttrId, $product->getEntityId());
            }
            return str_replace(
                array_keys($replace),
                array_values($replace),
                $this->stringContent
            );
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Dostępność produktu wg słownika CENEO
     * @param $product
     * @return int
     */
    private function getCeneoAvil($product)
    {
        if ($avil = $product->getResource()->getAttribute('ceneo_avil')->getFrontend()->getValue($product)) {
            return $avil;
        } else {
            return 99;
        }
    }

    /**
     * Aby ustawić produkty z zadmową wysyłką trzeba im nadać wagę 1000kg i ustwić w CENEO by produkty z taką wagą posiadały darmową wysyłkę
     *
     * @param $product
     * @return float|int
     */
    private function getCeneoFreeShipping($product)
    {
        $weight = $this->getWeight($product);
        if ((bool)$product->getData('ceneo_free_shipp') ) {
            return  1000;
        } else {
            return $weight;
        }
    }

    private function getEanCode($product)
    {
        $ean = 'get' . str_replace('_', '', ucwords($this->columnEan, '_'));
        if ($ean_ = $product->$ean()) {
            return $ean_;

        } else {
            return $product->getSku();
        }
    }

    /**
     * Waga produktu
     * @param $product
     * @return float|int
     */
    private function getWeight($product)
    {
        $weight = round($product->getWeight(), 2);
        $weight = (($weight > 0) ? $weight : 1);
        return $weight;
    }

    private function getFile()
    {
        $folder = (!empty($this->settings->getFolder())) ? $this->settings->getFolder() : 'feed';
        $filename = (!empty($this->settings->getFileName())) ? $this->settings->getFileName() : 'ceneo.xml';
        $path = $this->pubDir . DIRECTORY_SEPARATOR . $folder;
        $ceneo_file = $path . DIRECTORY_SEPARATOR . $filename;
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        if (file_exists($ceneo_file)) unlink($ceneo_file);
        return $ceneo_file;
    }

    private function getCategoryPath($product)
    {
        if ($ceneoCategory = $product->getCeneoKategoria()) {
            return $ceneoCategory;
        } else {
            $categoryCollection = clone $product->getCategoryCollection();
            $categoryCollection->clear();
            $categoryCollection->addAttributeToSort('level', $categoryCollection::SORT_ORDER_DESC)
                ->addAttributeToFilter('path', array('like' => "1/" . $this->storeManager->getStore()->getRootCategoryId() . "/%"));
            $categoryCollection->setPageSize(1);
            $category = $categoryCollection->getFirstItem();

            $breadcrumbCategories = $category->getParentCategories();
            $breadcrumbs = [];
            foreach ($breadcrumbCategories as $category) {
                $breadcrumbs[] = $category->getName();
            }
            return implode('/', $breadcrumbs);
        }

    }


    /**
     * Cena minimalna dla produktów konfigurowalnych
     * @param $product
     * @return float|int|mixed
     */
    private function getMinPrice($product)
    {
        try {
            if ($product->getTypeId() == 'configurable') {
                $child_products = $product->getTypeInstance()->getUsedProducts($product, null);
                if (count($child_products) > 0) {
                    $i = 1;
                    foreach ($child_products as $child) {
                        if ($i == 1) $price = $this->getMinPriceValue($child);
                        $productPrice = $this->getMinPriceValue($child);
                        $price = $price ? max($price, $productPrice) : $productPrice;
                        $i++;
                    }
                }
                return $price;
            } else {
                $price = $this->getMinPriceValue($product);
                return $price;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    private function getMinPriceValue($product)
    {
        try {
            $store = $product->getStore();
            $store->setCurrentCurrencyCode('PLN');

            $price = $product->getPriceInfo()->getPrice('final_price')->getValue();

            $rate = $this->taxCalculation->getCalculatedRate($product->getTaxClassId());

            if ($this->scopeConfig->getValue(
                    'tax/calculation/price_includes_tax',
                    ScopeInterface::SCOPE_STORE) == 1
            ) {
                // Product price in catalog is including tax.
                return $price;
            } else {
                // Product price in catalog is excluding tax.
                return $price + ($price * ($rate / 100));
            }
            return $rPrice;

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}