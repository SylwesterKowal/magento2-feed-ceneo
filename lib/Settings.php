<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 27.09.2018
 * Time: 12:33
 */

namespace Kowal\FeedCeneo\lib;


class Settings
{
    protected $scopeConfig;

    /**
     * Settings constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\State $state,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->state = $state;

        try {
            $state = $this->state->getAreaCode(); // getAreaCode() wywoła Exception jesli nie jest ustawiony
        } catch (\Exception $e) {
            $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML); // or \Magento\Framework\App\Area::AREA_FRONTEND
        }
    }

    public function getStatus()
    {
        return $this->scopeConfig->getValue('status/xml/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getHead()
    {
        return html_entity_decode($this->scopeConfig->getValue('ustawienia/xml/head', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
    }

    public function getContent()
    {
        return html_entity_decode($this->scopeConfig->getValue('ustawienia/xml/content', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
    }

    public function getFooter()
    {
        return html_entity_decode($this->scopeConfig->getValue('ustawienia/xml/footer', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
    }

    public function getKodEAN()
    {
        return html_entity_decode($this->scopeConfig->getValue('ustawienia/fields/ean', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
    }

    public function getFolder()
    {
        return $this->scopeConfig->getValue('status/xml/folder', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getFileName()
    {
        return $this->scopeConfig->getValue('status/xml/file_name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}