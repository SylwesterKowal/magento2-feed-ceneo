# FeedCeneo

Export pliku XML do Ceneo z Magento 2

## INSTALACJA

`php composer.har require kowal/module-feedceneo`

`php bin/magento setup:upgrade;`

`php bin/magento cache:flush;`

`php bin/magento cache:enable;`

## KONFIGURACJA

Moduł po zainstalowaniu dodaje kilka atrybutów, które należy uzupenić w produktach, eksportowanych do CENEO:

`ceneo_avil` - CENEO Dostępność

`ceneo_export` - CENEO Eksport

`ceneo_free_shipp` - CENEO Darmowa Wysyłka

### DODATKOWE ATRYBUTY

`ean` - Kod EAN

## RĘCZNE WYGENEROWANIE XML

`bin/magento kowal_feed:ceneo`