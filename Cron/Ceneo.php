<?php


namespace Kowal\FeedCeneo\Cron;

class Ceneo
{

    protected $logger;

    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Kowal\FeedCeneo\lib\Feed $feed
    )
    {
        $this->logger = $logger;
        $this->feed = $feed;

        parent::__construct();
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        $this->logger->addInfo("Start");
        $this->feed->run();
        $this->logger->addInfo("Koniec");
    }
}
